#!/usr/bin/env bash
set -aeuo pipefail

# gitlab-runner requires root
if [ "$(whoami)" != "root" ]; then
    echo "You need to be root to run this script."
fi

# create the dotenv file if it doesn't exist
if [ ! -f .env ]; then
    cp .env.default .env
    chmod 600 .env
    echo "Created new dotfile file at:"
    echo ""
    echo "    $(pwd)/.env"
    echo ""
fi

source .env.default
source .env

# create volumes 
mkdir -p volumes/ssh
mkdir -p volumes/composer
mkdir -p volumes/npm

cp ./artifacts/ssh/config ./volumes/ssh/config

# ensure ssh private key exists
if [ ! -f volumes/ssh/id_rsa ]; then
    echo "Expected SSH private key file at:"
    echo ""
    echo "   $(pwd)/volumes/ssh/id_rsa"
    echo ""
    exit 1
fi

# wipe out existing runners
# TODO: unregister existing runners first
cp ./artifacts/etc/gitlab-runner/config.toml.dist /etc/gitlab-runner/config.toml


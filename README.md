# GitLab CI Runner Registration Tool

This repository provides a little script that makes it super easy to spawn
runners.

## Prerequisites

- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner#install-gitlab-runner)
- Root access

## How to initialize gitlab-runner

```
./initialize
```
This will drop all runners and create build caches (Composer, NPM)

## How to spawn a runner

Executor: docker

```
./spawn-buildbox-php
```

Executor: dhell
```
./spawn-shell
```

